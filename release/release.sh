if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

echo Release version $1

cp ../target/generatorplugin-$1.jar .
cp ../target/generatorplugin-$1-javadoc.jar .
cp ../target/generatorplugin-$1-sources.jar .
cp ../pom.xml .
rm -rf bundle.jar

gpg -ab pom.xml
gpg -ab generatorplugin-$1.jar
gpg -ab generatorplugin-$1-javadoc.jar
gpg -ab generatorplugin-$1-sources.jar

jar -cfv bundle.jar generatorplugin-$1.jar generatorplugin-$1.jar.asc generatorplugin-$1-javadoc.jar generatorplugin-$1-javadoc.jar.asc generatorplugin-$1-sources.jar generatorplugin-$1-sources.jar.asc pom.xml pom.xml.asc

