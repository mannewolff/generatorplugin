package org.mwolff.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.mwolff.runner.XMLRunner;

/**
 */
@Mojo( name = "generate")
public class GeneratorMojo extends AbstractMojo
{
    @Parameter( property = "generatorProperties", defaultValue = "/Users/mwolff/gitlab/generatorexample/src/main/resources/mygenerator.properties" )
    private String generatorProperties;

    public void execute() throws MojoExecutionException
    {
        getLog().info( "----------------------------------------------" );
        getLog().info( "----------------Generating stuff--------------" );
        getLog().info( "----------------------------------------------" );
        getLog().info("Parameter: " + generatorProperties);
        String[] arguments = new String[]{generatorProperties};
        XMLRunner.main(arguments);
    }
}